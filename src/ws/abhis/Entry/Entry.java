package ws.abhis.Entry;

import java.io.File;
import java.util.List;

import ws.abhis.core.Diff;
import ws.abhis.reader.ReadFiles;

public class Entry {
	public static void main(String args[]) {
		String path = args[0];
		String siren = args[1];
		double siren1 = Double.parseDouble(siren);
		siren1 = siren1/100;
		
		ReadFiles objReadFiles = new ReadFiles();
		Diff objDiff = new Diff();
		final File directory = new File (path);
		List<File> allFiles = objReadFiles.listAllFiles(directory);
		
		//Print out file names for sanity check
		/*System.out.println("Files found");
		for (int i=0; i<allFiles.size(); i++) {
			System.out.println(allFiles.get(i).getName());
		}*/
		
		//System.out.println("\n");
		
		//Read each file and compare with the others
		System.out.println("Comparing files");
		for (int i=0; i<allFiles.size(); i++) {
			File file1 = allFiles.get(i);
			String[] file1Content = objReadFiles.readFileContent(file1);
			for (int j=0; j<allFiles.size(); j++) {
				if (i!=j) {
					File file2 = allFiles.get(j);
					String[] file2Content = objReadFiles.readFileContent(file2);
					double percentMatch = objDiff.findDiff(file1Content, file2Content);
					if(percentMatch >= siren1)
						System.out.println("Percent match between " + file1.getName() + " and " + file2.getName() + " is " + percentMatch);
				}
			}
		}
	}
}
