/*
 * Diff between two strings
 */
package ws.abhis.core;

public class Diff {
	public double findDiff(String[] x, String[] y) {
		int M = x.length;
		int N = y.length;
		
		int total = 0;
		
		int total1 = x.length;
		int total2 = y.length;
		
		if (total1 > total2) 
			total = total1;
		else
			total = total2;
		total = total1 + total2;
		
		int[][] opt = new int[M+1][N+1];
		
		for (int i = M-1; i>=0; i--) {
			for (int j = N-1; j>=0; j--) {
				if (x[i].equals(y[j])) 
					opt[i][j] = opt[i+1][j+1] + 1;
				else 
					opt[i][j] = Math.max(opt[i+1][j], opt[i][j+1]);
			}
		}
		
		int i = 0, j = 0;
		int nonMathching = 0;
		
		while (i<M && j<N) {
			
			if (x[i].equals(y[j])) {
				i++;
				j++;
			} else if (opt[i+1][j] >= opt[i][j+1]) {
				//System.out.println("< " + x[i++]);
				i++;
				
			}
			else {
				//System.out.println("> " + y[j++]);
				j++;
				
			}
			nonMathching++;
		}
		
		int matching = total -  nonMathching;
		
		double percent = (double)matching/total;
		//System.out.println(percent);
		return percent;
		
	}
}
