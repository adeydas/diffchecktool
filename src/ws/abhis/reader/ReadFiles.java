/*
 * Read all files in a directory
 */
package ws.abhis.reader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ReadFiles {
	public List<File> listAllFiles (final File directory) {
		
		List<File> allFiles = new ArrayList<File>();
		
		for (final File fileEntry : directory.listFiles()) {
			if(!fileEntry.isDirectory()) {
				allFiles.add(fileEntry);
			}
		}
		
		return allFiles;
		
	}
	
	public String[] readFileContent (final File fileName) {
		In inFile = new In(fileName);
		String[] ret = inFile.readAll().split("\\n");
		return ret;
	}
	
}
